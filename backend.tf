terraform {
  backend "s3" {
    bucket = "samarthbucket1205" # Replace this bucket name with your actual S3 bucket name
    key    = "Gitlab/terraform.tfstate"
    region = "ap-south-1"
  }
}
