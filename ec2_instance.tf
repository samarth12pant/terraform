resource "aws_instance" "jenkins_instance" {
  ami                    = "ami-0a0f1259dd1c90938" # the AMI ID changes as per the region, this is for Amazon Linux 2023 AMI from Mumbai Region
  instance_type          = "t2.medium"
  key_name               = "Key for Jenkins"                        # already created in the AWS Console
  vpc_security_group_ids = [aws_security_group.sg-for-jenkins.id]   # referencing to the SG from security_group.tf file
  user_data              = templatefile("./install_jenkins.sh", {}) # the script which we will be using to install jenkins on the ec2 instance

  tags = {
    "Name" = "Jenkins-Instance"
  }
  root_block_device {
    volume_size = 8
  }
}


