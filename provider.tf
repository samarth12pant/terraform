terraform {
  required_providers {
    aws = { # defining the source and version for AWS provider of terraform
      source  = "hashicorp/aws"
      version = "5.0"
    }
  }
}

provider "aws" {
  region = "ap-south-1" # this value is for the mumbai region
}
