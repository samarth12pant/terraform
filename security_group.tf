resource "aws_security_group" "sg-for-jenkins" {
  name        = "Security Group Jenkins"
  description = "For Allowing Traffic at port 8080,443,22,80"

  # ingress rule to allow traffic at 22, 443, 80, 8080
  ingress = [
    for port in [22, 443, 80, 8080] : {
      description      = "Ingress Ports for Ec2 Instance"
      from_port        = port
      to_port          = port
      protocol         = "tcp"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      security_groups  = []
      self             = false

    }
  ]

  # egress rule
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Security Group for Jenkins"
  }
}
